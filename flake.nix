{
  inputs = {
    nix-vscode-extensions.url = "github:nix-community/nix-vscode-extensions";
    nixpkgs.url = "github:NixOS/nixpkgs/release-23.11";
    purescript-overlay = {
      url = "github:thomashoneyman/purescript-overlay";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    mkSpagoDerivation.url = "github:jeslie0/mkSpagoDerivation";
  };

  outputs = { self, nix-vscode-extensions, nixpkgs, purescript-overlay, mkSpagoDerivation }:
    let
      supportedSystems = [
        "x86_64-linux"
        "x86_64-darwin"
        "aarch64-linux"
        "aarch64-darwin"
      ];
      forAllSystems = nixpkgs.lib.genAttrs supportedSystems;
      overlays = [
        mkSpagoDerivation.overlays.default
        nix-vscode-extensions.overlays.default
        purescript-overlay.overlays.default
      ];
      pkgsFor = system: import nixpkgs {
        inherit system overlays;
      };
    in
    {
      packages = forAllSystems (system:
        let
          pkgs = pkgsFor system;
          mkCaddyfile = root: pkgs.writeText "Caddyfile" ''
            :8080 {
              handle /api/* {
                reverse_proxy http://127.0.0.1:8081
              }
              handle {
                root * ${root}
                try_files {path} /index.html
                file_server
              }
              log
            }
          '';
        in
        {
          default = pkgs.mkSpagoDerivation {
            spagoYaml = ./spago.yaml;
            spagoLock = ./spago.lock;
            src = ./.;
            nativeBuildInputs = [
              pkgs.purs
              pkgs.purs-backend-es
              pkgs.spago-unstable
              pkgs.esbuild
            ];
            version = "0.1.0";
            buildPhase = "spago bundle --bundle-type app --platform browser --minify --outfile assets/index.js";
            installPhase = "mv assets $out";
          };
          backend-dev = pkgs.writeShellApplication {
            name = "backend-dev";
            runtimeInputs = [ pkgs.caddy ];
            text = ''
              RED='\e[7;31m'
              RESET='\e[0m'
              echo -ne "$RED"
              echo "Starting Caddy at http://127.0.0.1:8080"
              echo -n "Caddy will expect backend running at http://127.0.0.1:8081"
              echo -e "$RESET"
              caddy run --adapter caddyfile --config ${mkCaddyfile self.packages.${system}.default}
            '';
            checkPhase = "";
          };
          serve-assets = pkgs.writeShellApplication {
            name = "serve-assets";
            runtimeInputs = [ pkgs.caddy ];
            text = ''
              set -x
              caddy run --adapter caddyfile --config ${mkCaddyfile "./assets"}
            '';
            checkPhase = "";
          };
        });
      devShells = forAllSystems (system:
        let pkgs = pkgsFor system;
        in
        {
          default = pkgs.mkShell {
            buildInputs = [
              (pkgs.vscode-with-extensions.override rec {
                vscode = pkgs.vscodium;
                vscodeExtensions = with (pkgs.forVSCodeVersion vscode.version).vscode-marketplace; [
                  jnoortheen.nix-ide
                  nwolverson.ide-purescript
                  nwolverson.language-purescript
                  shardulm94.trailing-spaces
                ];
              })
              pkgs.esbuild
              pkgs.nodejs
              pkgs.purs
              pkgs.purs-backend-es
              pkgs.purs-tidy
              pkgs.spago-unstable
            ];
          };
        });
      legacyPackages = forAllSystems pkgsFor;
    };
}
