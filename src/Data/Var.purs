module Data.Var where

import Prelude

import Data.Newtype (class Newtype, unwrap)

class Get :: (Type -> Type) -> (Type -> Type) -> Constraint
class Get v m | v -> m where
  get :: forall a. v a -> m a

class Set :: (Type -> Type) -> (Type -> Type) -> Constraint
class Set v m | v -> m where
  set :: forall a. v a -> a -> m Unit

infix 0 set as $=

class Access :: (Type -> Type) -> (Type -> Type -> Type) -> (Type -> Type) -> Constraint
class Access v f v' | f -> v', v' -> f where
  access :: forall a b. v a -> f a b -> v' b

infixl 1 access as %

newtype VarRW m a = VarRW { get :: m a, set :: a -> m Unit }

derive instance Newtype (VarRW m a) _

instance Get (VarRW m) m where
  get = unwrap >>> _.get

instance Set (VarRW m) m where
  set = unwrap >>> _.set

newtype VarRO :: (Type -> Type) -> Type -> Type
newtype VarRO m a = VarRO { get :: m a }

derive instance Newtype (VarRO m a) _

instance Get (VarRO m) m where
  get = unwrap >>> _.get

newtype FieldRW :: (Type -> Type) -> Type -> Type -> Type
newtype FieldRW m a b = FieldRW { get :: a -> m b, set :: b -> a -> m Unit }

derive instance Newtype (FieldRW m a b) _

instance Bind m => Access (VarRW m) (FieldRW m) (VarRW m) where
  access (VarRW v) (FieldRW f) = VarRW { get: v.get >>= f.get, set: \x -> v.get >>= f.set x }

instance Bind m => Access (VarRO m) (FieldRW m) (VarRW m) where
  access (VarRO v) (FieldRW f) = VarRW { get: v.get >>= f.get, set: \x -> v.get >>= f.set x }

newtype FieldRO :: (Type -> Type) -> Type -> Type -> Type
newtype FieldRO m a b = FieldRO { get :: a -> m b }

instance Bind m => Access (VarRW m) (FieldRO m) (VarRO m) where
  access (VarRW v) (FieldRO f) = VarRO { get: v.get >>= f.get }

instance Bind m => Access (VarRO m) (FieldRO m) (VarRO m) where
  access (VarRO v) (FieldRO f) = VarRO { get: v.get >>= f.get }

derive instance Newtype (FieldRO m a b) _
