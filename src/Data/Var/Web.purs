module Data.Var.Web where

import Prelude

import Data.Maybe (Maybe, maybe)
import Data.Var (FieldRO(..), FieldRW(..), VarRO(..))
import Effect (Effect)
import Effect.Class (class MonadEffect, liftEffect)
import Web.HTML (Location, Window, window) as Web
import Web.HTML.Location (href, origin, pathname, setHref, setOrigin, setPathname) as Web
import Web.HTML.Window (localStorage, location) as Web
import Web.Storage.Storage (Storage, getItem, removeItem, setItem) as Web

varRO :: forall m a. MonadEffect m => Effect a -> VarRO m a
varRO get = VarRO { get: liftEffect get }

fieldRO :: forall m a b. MonadEffect m => (a -> Effect b) -> FieldRO m a b
fieldRO get = FieldRO { get: liftEffect <<< get }

fieldRW :: forall m a b. MonadEffect m => (a -> Effect b) -> (b -> a -> Effect Unit) -> FieldRW m a b
fieldRW get set = FieldRW { get: liftEffect <<< get, set: \x -> liftEffect <<< set x }

-- Window

window :: forall m. MonadEffect m => VarRO m Web.Window
window = varRO Web.window

-- Location

location :: forall m. MonadEffect m => FieldRO m Web.Window Web.Location
location = fieldRO Web.location

href :: forall m. MonadEffect m => FieldRW m Web.Location String
href = fieldRW Web.href Web.setHref

origin :: forall m. MonadEffect m => FieldRW m Web.Location String
origin = fieldRW Web.origin Web.setOrigin

pathname :: forall m. MonadEffect m => FieldRW m Web.Location String
pathname = fieldRW Web.pathname Web.setPathname

-- Local storage

localStorage :: forall m. MonadEffect m => FieldRO m Web.Window Web.Storage
localStorage = fieldRO Web.localStorage

at :: forall m. MonadEffect m => String -> FieldRW m Web.Storage (Maybe String)
at key = fieldRW (Web.getItem key) setOrRemoveItem
  where
  setOrRemoveItem = maybe (Web.removeItem key) (Web.setItem key)
