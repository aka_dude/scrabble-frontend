module Data.NonZero where

import Prelude

import Data.Maybe (Maybe(..))
import Data.Newtype (class Newtype)

newtype NonZero a = NonZero a

derive instance Newtype (NonZero a) _

derive instance Functor NonZero

instance Semiring a => Semigroup (NonZero a) where
  append (NonZero x) (NonZero y) = NonZero $ x + y

increment :: forall a. Semiring a => Maybe (NonZero a) -> Maybe (NonZero a)
increment Nothing = Just $ NonZero one
increment (Just x) = Just $ x <#> (_ + one)

substract :: forall a. Ring a => Eq a => NonZero a -> NonZero a -> Maybe (NonZero a)
substract (NonZero x) (NonZero y) =
  let
    z = x - y
  in
    if z == zero then Nothing
    else Just $ NonZero z

-- decrement Nothing = Nothing
-- decrement (Just x) =
