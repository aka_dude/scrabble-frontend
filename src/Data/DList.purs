module Data.DList where

import Prelude

import Data.Foldable (class Foldable, foldMapDefaultL)
import Data.Generic.Rep (class Generic)
import Data.Lens (Lens', lens)
import Data.List.Lazy (cons, nil, reverse, toUnfoldable) as List
import Data.Map (Map)
import Data.Map (alter, delete, empty, insert, lookup) as Map
import Data.Maybe (Maybe(..))
import Data.Newtype (class Newtype, over)
import Data.Show.Generic (genericShow)
import Data.Tuple.Nested ((/\))
import Data.Unfoldable (class Unfoldable, class Unfoldable1)
import Partial.Unsafe (unsafeCrashWith)

newtype DList a = DList
  { values :: Map (Elem a) { value :: a, prev :: Elem a, next :: Elem a }
  -- TODO: maybe use Maybe (Elem a) for references?
  -- ^ for head, `prev` points at itself, analogous for tail
  , ends :: Maybe { head :: (Elem a), last :: (Elem a) }
  , nextAvailableAddress :: Elem a
  }

derive instance Generic (DList a) _

instance Show a => Show (DList a) where
  show = genericShow

instance Foldable DList where
  foldr f acc xs@(DList { values }) = go acc (eback xs)
    where
    go acc' Nothing = acc'
    go acc' (Just p) = case values # Map.lookup p of
      Nothing -> unsafeCrashWith "Data.DList.foldr: reference is invalid"
      Just { value, prev } -> go (f value acc') $ if p == prev then Nothing else Just prev

  foldl f acc xs@(DList { values }) = go acc (efront xs)
    where
    go acc' Nothing = acc'
    go acc' (Just p) = case values # Map.lookup p of
      Nothing -> unsafeCrashWith "Data.DList.foldl: reference is invalid"
      Just { value, next } -> go (f acc' value) $ if p == next then Nothing else Just next

  foldMap f = foldMapDefaultL f

instance Unfoldable DList where
  unfoldr f = go empty
    where
    go res b = case f b of
      Nothing -> res
      Just (a /\ b') -> go (pushBack a res) b'

instance Unfoldable1 DList where
  unfoldr1 f = go empty
    where
    go res b = case f b of
      (a /\ Nothing) -> pushBack a res
      (a /\ Just b') -> go (pushBack a res) b'

newtype Elem :: Type -> Type
newtype Elem a = Elem Int

derive instance Newtype (Elem a) _
derive instance Generic (Elem a) _
derive newtype instance Eq (Elem a)
derive newtype instance Ord (Elem a)

instance Show (Elem a) where
  show = genericShow

-- TODO: this should be method of Plus
empty :: forall a. DList a
empty = DList { values: Map.empty, ends: Nothing, nextAvailableAddress: Elem 0 }

pushFront :: forall a. a -> DList a -> DList a
pushFront value (DList { values, ends: Nothing, nextAvailableAddress }) = DList
  { values: Map.insert nextAvailableAddress { value, prev: nextAvailableAddress, next: nextAvailableAddress }
      values
  , ends: Just { head: nextAvailableAddress, last: nextAvailableAddress }
  , nextAvailableAddress: over Elem (_ + 1) nextAvailableAddress
  }
pushFront value (DList { values, ends: Just { head, last }, nextAvailableAddress }) = DList
  { values: values
      # Map.insert nextAvailableAddress { value, prev: nextAvailableAddress, next: head }
      # Map.alter (map \head' -> head' { prev = nextAvailableAddress }) head
  , ends: Just { head: nextAvailableAddress, last }
  , nextAvailableAddress: over Elem (_ + 1) nextAvailableAddress
  }

pushBack :: forall a. a -> DList a -> DList a
pushBack value (DList { values, ends: Nothing, nextAvailableAddress }) = DList
  { values: Map.insert nextAvailableAddress { value, prev: nextAvailableAddress, next: nextAvailableAddress }
      values
  , ends: Just { head: nextAvailableAddress, last: nextAvailableAddress }
  , nextAvailableAddress: over Elem (_ + 1) nextAvailableAddress
  }
pushBack value (DList { values, ends: Just { head, last }, nextAvailableAddress }) = DList
  { values: values
      # Map.insert nextAvailableAddress { value, prev: last, next: nextAvailableAddress }
      # Map.alter (map \last' -> last' { next = nextAvailableAddress }) last
  , ends: Just { head, last: nextAvailableAddress }
  , nextAvailableAddress: over Elem (_ + 1) nextAvailableAddress
  }

efront :: forall a. DList a -> Maybe (Elem a)
efront (DList { ends }) = ends <#> _.head

eback :: forall a. DList a -> Maybe (Elem a)
eback (DList { ends }) = ends <#> _.last

evalue :: forall a. Elem a -> DList a -> Maybe a
evalue p (DList { values }) = Map.lookup p values <#> _.value

enext :: forall a. Elem a -> DList a -> Maybe (Elem a)
enext p (DList { values }) = Map.lookup p values >>= \{ next } -> if next == p then Nothing else Just next

eprev :: forall a. Elem a -> DList a -> Maybe (Elem a)
eprev p (DList { values }) = Map.lookup p values >>= \{ prev } -> if prev == p then Nothing else Just prev

-- | `remove p >>> remove p = remove p`
eremove :: forall a. Elem a -> DList a -> DList a
eremove _ r@(DList { ends: Nothing }) = r
eremove p (DList { values, ends: ends@(Just { head, last }), nextAvailableAddress }) =
  case Map.lookup p values of
    Nothing -> DList { values, ends, nextAvailableAddress }
    Just { prev, next } ->
      DList
        { values: values # Map.delete p # updatePrev prev next # updateNext next prev
        , ends: mkEnds prev next
        , nextAvailableAddress
        }
  where
  updatePrev prev next
    | prev == p = identity -- `p` is a prev edge
    | otherwise = Map.alter (map \prev' -> prev' { next = if last == p then prev else next }) prev
  updateNext next prev
    | next == p = identity -- `p` is a next edge
    | otherwise = Map.alter (map \next' -> next' { prev = if head == p then next else prev }) next
  mkEnds prev next
    | head == p && last == p = Nothing
    | otherwise = Just { head: if head == p then next else head, last: if last == p then prev else last }

eupdate :: forall a. Elem a -> a -> DList a -> DList a
eupdate p value (DList r@{ values }) = DList r
  { values = values # Map.alter (map \v -> v { value = value }) p }

einsertBefore :: forall a. Elem a -> a -> DList a -> DList a
einsertBefore _ value (DList { ends: Nothing }) = pushFront value empty
einsertBefore p value r@(DList { values, ends: Just { head, last }, nextAvailableAddress })
  | p == head = pushFront value r
  | otherwise = case values # Map.lookup p of
      Just p' -> DList
        { values: values
            # Map.insert nextAvailableAddress { value, prev: p'.prev, next: p }
            # Map.insert p p' { prev = nextAvailableAddress }
            # Map.alter (map \prev' -> prev' { next = nextAvailableAddress }) p'.prev
        , ends: Just { head, last }
        , nextAvailableAddress: over Elem (_ + 1) nextAvailableAddress
        }
      Nothing -> unsafeCrashWith "Data.DList.einsertBefore: Elem points at non-existent value"

einsertAfter :: forall a. Elem a -> a -> DList a -> DList a
einsertAfter _ value (DList { ends: Nothing }) = pushFront value empty
einsertAfter p value r@(DList { values, ends: Just { head, last }, nextAvailableAddress })
  | p == last = pushBack value r
  | otherwise = case values # Map.lookup p of
      Just p' -> DList
        { values: values
            # Map.insert nextAvailableAddress { value, prev: p, next: p'.next }
            # Map.insert p p' { next = nextAvailableAddress }
            # Map.alter (map \next' -> next' { prev = nextAvailableAddress }) p'.next
        , ends: Just { head, last }
        , nextAvailableAddress: over Elem (_ + 1) nextAvailableAddress
        }
      Nothing -> unsafeCrashWith "Data.DList.einsertAfter: reference is invalid"

elems :: forall f a. Unfoldable f => DList a -> f (Elem a)
elems xs = List.toUnfoldable $ List.reverse $ go List.nil $ efront xs
  where
  go acc Nothing = acc
  go acc (Just p) = go (List.cons p acc) $ enext p xs

mut :: forall a. Elem a -> Lens' (DList a) (Maybe a)
mut p = lens (evalue p) setter
  where
  setter xs Nothing = eremove p xs
  setter xs (Just x) = eupdate p x xs

after :: forall a. Elem a -> Lens' (DList a) (Maybe a)
after p = lens (const Nothing) setter
  where
  setter xs Nothing = xs
  setter xs (Just x) = einsertAfter p x xs

front :: forall a. Lens' (DList a) (Maybe a)
front = lens (const Nothing) setter
  where
  setter xs Nothing = xs
  setter xs (Just x) = pushFront x xs
