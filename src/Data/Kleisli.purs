module Data.Kleisli where

import Data.Newtype (class Newtype, unwrap)

newtype Kleisli :: forall k. (k -> Type) -> Type -> k -> Type
newtype Kleisli m a b = Kleisli (a -> m b)

derive instance Newtype (Kleisli m a b) _

unKleisli :: forall m a b. Kleisli m a b -> (a -> m b)
unKleisli = unwrap
