module Data.MultiSet where

import Prelude

import Data.Foldable (class Foldable, foldMap, foldlDefault, foldr, foldrDefault)
import Data.List.Lazy (List)
import Data.Map (Map)
import Data.Map (alter, catMaybes, difference, empty, intersectionWith, toUnfoldable, union, unionWith) as Map
import Data.Maybe (Maybe)
import Data.Newtype (class Newtype)
import Data.NonZero (NonZero(..))
import Data.NonZero (increment, substract) as NonZero
import Data.Tuple.Nested ((/\))
import Data.Unfoldable (class Unfoldable, replicate)

newtype MultiSet a = MultiSet (Map a (NonZero Int))

derive instance Newtype (MultiSet a) _

instance Ord a => Semigroup (MultiSet a) where
  append (MultiSet xs) (MultiSet ys) = MultiSet $ Map.unionWith (<>) xs ys

instance Ord a => Monoid (MultiSet a) where
  mempty = MultiSet Map.empty

instance Foldable MultiSet where
  foldl f = foldlDefault f
  foldr f = foldrDefault f
  foldMap f xs = foldMap f (toUnfoldable xs :: List _)

insert :: forall a. Ord a => a -> MultiSet a -> MultiSet a
insert x (MultiSet xs) = MultiSet $ Map.alter NonZero.increment x xs

difference :: forall a. Ord a => MultiSet a -> MultiSet a -> MultiSet a
difference (MultiSet xs) (MultiSet ys) = MultiSet $ Map.difference xs ys `Map.union` _Map_intersectionWith'
  NonZero.substract
  xs
  ys

infixl 6 difference as \\

_Map_intersectionWith' :: forall k a b c. Ord k => (a -> b -> Maybe c) -> Map k a -> Map k b -> Map k c
_Map_intersectionWith' f xs ys = Map.catMaybes $ Map.intersectionWith f xs ys

fromFoldable :: forall f a. Foldable f => Ord a => f a -> MultiSet a
fromFoldable = foldr insert mempty

toUnfoldable :: forall f a. Unfoldable f => Bind f => MultiSet a -> f a
toUnfoldable (MultiSet xs) = Map.toUnfoldable xs >>= \(x /\ NonZero n) -> replicate n x
