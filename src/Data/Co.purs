module Data.Co where

import Data.Newtype (class Newtype, unwrap)

newtype Co :: forall k1 k2. (k1 -> k2 -> Type) -> k2 -> k1 -> Type
newtype Co f a b = Co (f b a)

derive instance Newtype (Co f a b) _

unCo :: forall f a b. Co f a b -> f b a
unCo = unwrap
