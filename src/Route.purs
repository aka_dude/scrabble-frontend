module Route where

import Prelude

import Api (GameId)
import Control.Invertible (ieof, iwrapped, (%>), (++), (:+))
import Control.Invertible.List (InvListParser, iliteral, iparse, irender, ishow)
import Control.Parser.List (pint)
import Data.Array (fromFoldable, toUnfoldable) as Array
import Data.Either.Nested (type (\/))
import Data.Foldable (foldMap)
import Data.List.Lazy (filter) as List
import Data.String (Pattern(..)) as String
import Data.String.Common (null, split) as String
import Data.Variant (Variant)
import Data.Variant (inj) as Variant
import Type.Prelude (Proxy(..))

type Route = Variant (index :: {}, game :: GameId)

index :: forall r. Variant (index :: {} | r)
index = Variant.inj (Proxy :: Proxy "index") {}

game :: forall a r. a -> Variant (game :: a | r)
game = Variant.inj (Proxy :: Proxy "game")

parse :: String -> Array String \/ Route
parse = String.split (String.Pattern "/") >>> Array.toUnfoldable >>> List.filter (not <<< String.null) >>>
  flip iparse iroute

render :: Route -> String
render = foldMap ("/" <> _) <<< Array.fromFoldable <<< flip irender iroute

igameId :: InvListParser String GameId
igameId = iwrapped (ishow pint)

iroute :: InvListParser String Route
iroute = (Proxy :: Proxy "index") :+ ieof
  ++ ((Proxy :: Proxy "game") :+ (iliteral "game" %> igameId))
