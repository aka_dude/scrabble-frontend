module Control.Parser
  ( class Parser
  , peof
  , ptoken
  , pcontext
  , pempty
  ) where

import Prelude

class Parser p token | p -> token where
  -- | Succeed iff end of input
  peof :: p Unit
  -- | Get one token
  ptoken :: p token
  -- | Provide context for errors
  pcontext :: forall a. String -> p a -> p a

pempty :: forall p. Applicative p => p Unit
pempty = pure unit
