module Control.Invertible where

import Prelude

import Control.Alt (class Alt, (<|>))
import Control.Parser (class Parser, pcontext, peof)
import Data.Decide (class Decide, choose)
import Data.Divide (class Divide, divide)
import Data.Divisible (class Divisible, conquer)
import Data.Either (Either(..))
import Data.Functor.Contravariant (class Contravariant, (>#<))
import Data.Maybe (Maybe(..))
import Data.Newtype (class Newtype, unwrap, wrap)
import Data.Profunctor.Strong ((&&&))
import Data.Tuple.Nested ((/\))
import Data.Variant (case_, contract, expand, inj, on) as Variant
import Data.Variant (class Contractable, Variant)
import Data.Variant.Internal (impossible) as Variant
import Prim.Row (class Cons, class Nub, class Union)
import Prim.RowList (class RowToList)
import Record (disjointUnion, get, insert) as Record
import Record.Extra (class Keys, pick) as Record
import Type.Prelude (class IsSymbol, Proxy, reflectSymbol)

{-
TODO: add `s` parameter:
type Invertible p r s a = { parser :: p s a, renderer :: r a s }
Q: What comes instead of Contravariant?
A: Maybe have `r s a`, and "abuse" symmetry of Invertible?
-}
type Invertible :: forall k. (k -> Type) -> (k -> Type) -> k -> Type
type Invertible p r a = { parser :: p a, renderer :: r a }

iempty :: forall p r. Applicative p => Divisible r => Invertible p r {}
iempty = { parser, renderer }
  where
  parser = pure {}
  renderer = conquer

ieof :: forall p t r. Functor p => Parser p t => Divisible r => Invertible p r {}
ieof = { parser, renderer }
  where
  parser = {} <$ peof
  renderer = conquer

iwrapped ::
  forall p r a n. Functor p => Contravariant r => Newtype n a => Invertible p r a -> Invertible p r n
iwrapped a = { parser, renderer }
  where
  parser = a.parser <#> wrap
  renderer = a.renderer >#< unwrap

record ::
  forall f w p t r a.
  Functor p =>
  Parser p t =>
  Contravariant r =>
  IsSymbol f =>
  Cons f a () w =>
  Proxy f ->
  Invertible p r a ->
  Invertible p r (Record w)
record f a = { parser, renderer }
  where
  parser = pcontext ("Parsing variant " <> show (reflectSymbol f)) a.parser <#> \x -> Record.insert f x {}
  renderer = a.renderer >#< Record.get f

variant ::
  forall f w p t r a.
  Functor p =>
  Parser p t =>
  Contravariant r =>
  IsSymbol f =>
  Cons f a () w =>
  Proxy f ->
  Invertible p r a ->
  Invertible p r (Variant w)
variant f a = { parser, renderer }
  where
  parser = pcontext ("Parsing variant " <> show (reflectSymbol f)) a.parser <#> Variant.inj f
  renderer = a.renderer >#< Variant.on f identity Variant.case_

infixr 5 record as :*
infixr 4 variant as :+

both ::
  forall p r a b c al bl.
  Apply p =>
  Divide r =>
  Union a b c =>
  Union b a c =>
  Nub c c =>
  RowToList a al =>
  Record.Keys al =>
  RowToList b bl =>
  Record.Keys bl =>
  Invertible p r (Record a) ->
  Invertible p r (Record b) ->
  Invertible p r (Record c)
both a b = { renderer, parser }
  where
  parser = Record.disjointUnion <$> a.parser <*> b.parser
  renderer = divide (Record.pick &&& Record.pick) a.renderer b.renderer

either ::
  forall p r a b c.
  Alt p =>
  Decide r =>
  Union a b c =>
  Union b a c =>
  Contractable c a =>
  Contractable c b =>
  Invertible p r (Variant a) ->
  Invertible p r (Variant b) ->
  Invertible p r (Variant c)
either a b = { parser, renderer }
  where
  parser = (Variant.expand <$> a.parser) <|> (Variant.expand <$> b.parser)
  renderer = choose chooser a.renderer b.renderer
  chooser z = case Variant.contract z of
    Just x -> Left x
    Nothing -> case Variant.contract z of
      Just y -> Right y
      Nothing -> Variant.impossible
        "Control.Invertible.either: Variant.contract failed to choose between `a` and `b`"

infixr 3 both as **
infixr 2 either as ++

sequence ::
  forall p r a.
  Apply p =>
  Divide r =>
  Invertible p r Unit ->
  Invertible p r a ->
  Invertible p r a
sequence e a = { parser, renderer }
  where
  parser = e.parser *> a.parser
  renderer = divide (unit /\ _) e.renderer a.renderer

infixr 3 sequence as %>
