module Control.Parser.List where

import Prelude

import Control.Alt (class Alt)
import Control.Monad.Error.Class (class MonadThrow)
import Control.Monad.Except (class MonadError, ExceptT, catchError, runExceptT, throwError)
import Control.Monad.State (StateT, get, gets, put, runStateT)
import Control.Parser (class Parser, pcontext, peof, ptoken)
import Data.Either.Nested (type (\/))
import Data.Identity (Identity)
import Data.Int (fromString) as Int
import Data.List.Lazy (List, null, uncons)
import Data.Maybe (Maybe(..), maybe)
import Data.Newtype (class Newtype, unwrap)
import Data.Tuple (snd, swap)
import Data.Tuple.Nested (type (/\))

newtype ListParserT :: (Type -> Type) -> Type -> Type -> Type
newtype ListParserT m s a = ListParserT (StateT (List s) (ExceptT (Array String) m) a)

runListParserT :: forall s m a. Monad m => List s -> ListParserT m s a -> m ((Array String) \/ (List s /\ a))
runListParserT src = unwrap >>> flip runStateT src >>> map swap >>> runExceptT

runListParser :: forall s a. List s -> ListParserT Identity s a -> Array String \/ a
runListParser src p = p <* peof # runListParserT src # unwrap # map snd

derive instance Newtype (ListParserT m s a) _
derive newtype instance Functor m => Functor (ListParserT m s)
derive newtype instance Monad m => Apply (ListParserT m s)
derive newtype instance Monad m => Applicative (ListParserT m s)
derive newtype instance Monad m => Alt (ListParserT m s)
derive newtype instance Monad m => Bind (ListParserT m s)
derive newtype instance Monad m => Monad (ListParserT m s)

derive newtype instance Monad m => MonadThrow (Array String) (ListParserT m s)
derive newtype instance Monad m => MonadError (Array String) (ListParserT m s)

instance Monad m => Parser (ListParserT m s) s where
  peof = ListParserT do
    gets null >>= if _ then pure unit else throwError [ "ListParserT.peof: Expected end of input" ]
  ptoken = ListParserT do
    get >>= uncons >>> case _ of
      Nothing -> throwError [ "ListParserT.ptoken: Expected a token" ]
      Just { head, tail } -> do
        put tail
        pure head
  pcontext ctxt p = p `catchError` (throwError <<< map \e -> ctxt <> ": " <> e)

pliteral :: forall m a. Monad m => Eq a => Show a => a -> ListParserT m a Unit
pliteral x = do
  y <- ptoken
  when (x /= y) do
    ListParserT $ throwError [ "Expected literal " <> show x ]

pint :: forall m. Monad m => ListParserT m String Int
pint = pcontext "pint" do
  ptoken >>= Int.fromString >>> maybe (throwError [ "Failed to parse int" ]) pure
