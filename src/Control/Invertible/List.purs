module Control.Invertible.List where

import Prelude

import Control.Invertible (Invertible)
import Control.Parser (ptoken)
import Control.Parser.List (ListParserT, pliteral, runListParser)
import Data.Decide (class Decide)
import Data.Divide (class Divide)
import Data.Divisible (class Divisible)
import Data.Either.Nested (type (\/))
import Data.Function (applyFlipped)
import Data.Functor.Contravariant (class Contravariant)
import Data.Identity (Identity)
import Data.List.Lazy (List)
import Data.Newtype (class Newtype, unwrap)
import Data.Profunctor.Choice ((|||))
import Data.Profunctor.Strong ((***))
import Data.Tuple (uncurry)

type InvListParser s a = Invertible (ListParserT Identity s) (ListRenderer s) a

iparse :: forall s a. List s -> InvListParser s a -> Array String \/ a
iparse src = _.parser >>> runListParser src

irender :: forall s a. a -> InvListParser s a -> List s
irender x = _.renderer >>> unwrap >>> applyFlipped x

newtype ListRenderer s a = ListRenderer (a -> List s)

derive instance Newtype (ListRenderer s a) _
derive instance Contravariant (ListRenderer s)

instance Divide (ListRenderer s) where
  divide f (ListRenderer fb) (ListRenderer fc) = ListRenderer (f >>> (fb *** fc) >>> uncurry append)

instance Divisible (ListRenderer a) where
  conquer = ListRenderer \_ -> mempty

instance Decide (ListRenderer s) where
  choose f (ListRenderer fb) (ListRenderer fc) = ListRenderer (f >>> (fb ||| fc))

iliteral :: forall s. Eq s => Show s => s -> InvListParser s Unit
iliteral x = { parser, renderer }
  where
  parser = pliteral x
  renderer = ListRenderer \_ -> pure x

iany :: forall s. InvListParser s s
iany = { parser, renderer }
  where
  parser = ptoken
  renderer = ListRenderer pure

ishow :: forall a. Show a => ListParserT Identity String a -> InvListParser String a
ishow parser = { parser, renderer: ListRenderer $ pure <<< show }

{-
host = Proxy :: Proxy "host"
preferences = Proxy :: Proxy "preferences"
game = Proxy :: Proxy "game"
gameId = Proxy :: Proxy "gameId"

example1 = (host :+ ieof)
  ++ (preferences :+ (iliteral "preferences" %> iany))
  ++ (game :+ (iliteral "game" %> record gameId iany))
-}
