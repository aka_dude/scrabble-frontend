module Halogen.Error where

import Prelude

import Control.Monad.Except (ExceptT, runExceptT)
import Control.Monad.State (get, modify_)
import Data.Array (catMaybes)
import Data.Either (either)
import Data.Lens (Fold', Lens', Setter', (.~), (^?))
import Data.Lens.Record as Lens
import Data.Maybe (Maybe(..))
import Data.Maybe.First (First)
import Data.Newtype (class Newtype)
import Data.Newtype (modify) as Newtype
import Effect.Class (class MonadEffect)
import Effect.Class.Console (logShow, warn)
import Halogen (HalogenM, ComponentHTML) as H
import Halogen.HTML (div, div_, text) as HH
import Halogen.HTML.Properties (style) as HP
import Type.Prelude (Proxy(..))

type State action rest = { error :: Maybe { message :: String, action :: action } | rest }

render ::
  forall m state action slots.
  Fold' (First String) state String ->
  (state -> H.ComponentHTML action slots m) ->
  (state -> H.ComponentHTML action slots m)
render g inner st = HH.div_ $ catMaybes [ errorMaybe $ st ^? g, Just (inner st) ]
  where
  errorMaybe = map \message -> HH.div [ HP.style "background-color: red" ]
    [ HH.text $ "Error: " <> message ]

type M rest action = H.HalogenM (State action rest) action

handleAction ::
  forall action rest slots output m.
  (action -> ExceptT String (M rest action slots output m) Unit) ->
  (action -> M rest action slots output m Unit)
handleAction inner action = runExceptT (inner action) >>= flip either pure \message -> do
  modify_ \s -> s { error = Just { message, action } }

wrapHalogen ::
  forall action rest slots output m.
  action ->
  ExceptT String (M rest action slots output m) Unit ->
  M rest action slots output m Unit
wrapHalogen def m = runExceptT m >>= flip either pure \message -> do
  modify_ \s -> s { error = Just { message, action: def } }

handleActionNoAction ::
  forall state action slots output m.
  MonadEffect m =>
  Show state =>
  Setter' state (Maybe { message :: String }) ->
  ( action ->
    ExceptT String (H.HalogenM state action slots output m) Unit
  ) ->
  (action -> H.HalogenM state action slots output m Unit)
handleActionNoAction i inner action = runExceptT (inner action) >>= flip either pure \message -> do
  warn $ "User-generated error: " <> message
  modify_ $ i .~ Just { message }
  logShow =<< get

wrapHalogenNoAction ::
  forall state action rest slots output m.
  Newtype state { error :: Maybe { message :: String } | rest } =>
  ExceptT String (H.HalogenM state action slots output m) Unit ->
  H.HalogenM state action slots output m Unit
wrapHalogenNoAction m = runExceptT m >>= flip either pure \message -> do
  modify_ $ Newtype.modify _ { error = Just { message } }

_error :: forall a r. Lens' { error :: a | r } a
_error = Lens.prop (Proxy :: Proxy "error")

_message :: forall a r. Lens' { message :: a | r } a
_message = Lens.prop (Proxy :: Proxy "message")
