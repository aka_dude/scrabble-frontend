module Halogen.Simple where

import Prelude

import Data.Newtype (class Newtype)
import Halogen as H

newtype Action state slots m a = A (H.HalogenM state (Action state slots m a) slots Void m a)

derive instance Newtype (Action state slots m a) _
