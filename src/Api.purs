module Api where

import Prelude

import Affjax.RequestBody as RequestBody
import Affjax.RequestHeader (RequestHeader(..))
import Affjax.ResponseFormat (json) as Affjax
import Affjax.ResponseHeader (ResponseHeader(..))
import Affjax.Web (Error, Response, defaultRequest, printError, request) as Affjax
import Control.Monad.Error.Class (class MonadThrow, throwError)
import Control.Monad.Reader (class MonadAsk, ask)
import Data.Argonaut (class DecodeJson, class EncodeJson, Json, JsonDecodeError, decodeJson, encodeJson, printJsonDecodeError)
import Data.Either (Either(..))
import Data.Either.Nested ((\/))
import Data.Foldable (findMap)
import Data.HTTP.Method (Method(..))
import Data.Maybe (Maybe(..), fromMaybe, maybe)
import Data.MediaType (MediaType(..))
import Data.Newtype (class Newtype, unwrap)
import Data.String (CodePoint)
import Data.String (toLower) as String
import Data.Unfoldable (fromMaybe) as Unfoldable
import Data.Variant (Variant)
import Effect.Aff.Class (class MonadAff, liftAff)
import Partial.Unsafe (unsafePartial)
import Record (insert) as Record
import Type.Prelude (Proxy(..))
import Utils (chunks, shead, (!))

type Error r = Variant (eRequest :: Affjax.Error, eParseJson :: JsonDecodeError | r)

request ::
  forall m body.
  EncodeJson body =>
  MonadAff m =>
  MonadThrow String m =>
  MonadAsk Ctxt m =>
  Method ->
  String ->
  RequestOpts body ->
  m (Affjax.Response Json)
request method path { body, token } = do
  { api: { root } } <- ask
  let
    r = Affjax.defaultRequest
      { url = root <> path
      , method = Left method
      , headers = [ ContentType (MediaType "application/json") ] <> Unfoldable.fromMaybe
          (token <#> unwrap >>> RequestHeader "Authorization")
      , responseFormat = Affjax.json
      , content = body <#> encodeJson >>> RequestBody.Json
      }
  response <- liftAff (Affjax.request r) >>= throwError <<< Affjax.printError \/ pure
  if 200 <= unwrap response.status && unwrap response.status < 300 then
    pure response
  else do
    let
      e =
        "Server returned error: "
          <> show (unwrap response.status)
          <> " "
          <> response.statusText
          <>
            case decodeJson response.body of
              Right ({ error } :: { error :: String }) -> " (" <> error <> ")"
              Left _ -> ""
    throwError e

type Ctxt = { api :: { root :: String } }

type RequestOpts body =
  { body :: Maybe body
  , token :: Maybe Token
  }

fieldSide :: Int
fieldSide = 15

type Game =
  { id :: GameId
  -- , letter_bag :: Maybe (Array String)
  -- , winning_score :: Maybe Int
  -- , hand_size :: Maybe Int
  -- , language :: Maybe String
  , current_turn :: Int
  , field :: Array String
  , game_state :: String -- "in_lobby" | ...
  , players :: Array Player
  , players_turn :: Int
  , winners :: Array PlayerId
  }

gameField :: Array String -> Array (Array (Maybe CodePoint))
gameField = chunks fieldSide >>> map (map shead)

gameLetterAt :: Int -> Int -> Array String -> Maybe CodePoint
gameLetterAt i j field = shead $ unsafePartial $ field ! (i * fieldSide + j)

newtype GameId = GameId Int

derive instance Newtype GameId _
derive newtype instance DecodeJson GameId
derive newtype instance EncodeJson GameId
derive newtype instance Eq GameId
derive newtype instance Ord GameId
derive newtype instance Show GameId

type Player =
  { id :: PlayerId
  , nickname :: String
  , score :: Int
  , -- | False for people that left the game (with `/leave`)
    active_player :: Boolean
  , hand :: Array String
  }

newtype PlayerId = PlayerId Int

derive newtype instance DecodeJson PlayerId
derive newtype instance Eq PlayerId
derive newtype instance Show PlayerId

newtype Token = Token String

derive instance Newtype Token _
derive newtype instance DecodeJson Token
derive newtype instance EncodeJson Token
derive newtype instance Show Token

newGame ::
  forall m.
  MonadAff m =>
  MonadThrow String m =>
  MonadAsk Ctxt m =>
  { nickname :: String } ->
  m { game :: Game, token :: Token }
newGame body = do
  response <- request POST "/new_game" { body: Just body, token: Nothing }
  result <- decodeJson response.body # throwError <<< printJsonDecodeError \/ pure
  token <- response.headers
    # findMap
        ( \(ResponseHeader name value) -> if String.toLower name == "token" then Just value else Nothing
        )
    # maybe (throwError "No Token Header") pure
  pure $ Record.insert (Proxy :: Proxy "token") (Token token) result

joinGame ::
  forall m.
  MonadAff m =>
  MonadThrow String m =>
  MonadAsk Ctxt m =>
  { nickname :: String, gameId :: GameId } ->
  m { game :: Game, token :: Token }
joinGame { nickname, gameId } = do
  response <- request POST ("/join_game/" <> show gameId) { body: Just { nickname }, token: Nothing }
  result <- decodeJson response.body # throwError <<< printJsonDecodeError \/ pure
  token <- response.headers
    # findMap
        ( \(ResponseHeader name value) -> if String.toLower name == "token" then Just value else Nothing
        )
    # maybe (throwError "No Token Header") pure
  pure $ Record.insert (Proxy :: Proxy "token") (Token token) result

startGame ::
  forall m.
  MonadAff m =>
  MonadThrow String m =>
  MonadAsk Ctxt m =>
  { token :: Token, hand_size :: Maybe Int } ->
  m { game :: Game }
startGame { token, hand_size } = do
  response <- request POST "/start_game"
    { body: Just { language: "rus", hand_size: hand_size # fromMaybe 12, winning_score: 100 }
    , token: Just token
    }
  decodeJson response.body # throwError <<< printJsonDecodeError \/ pure

showGame ::
  forall m.
  MonadAff m =>
  MonadThrow String m =>
  MonadAsk Ctxt m =>
  { token :: Token } ->
  m { game :: Game }
showGame { token } = do
  response <- request GET "/show" { body: Nothing :: Maybe {}, token: Just token }
  decodeJson response.body # throwError <<< printJsonDecodeError \/ pure

list ::
  forall m.
  MonadAff m =>
  MonadThrow String m =>
  MonadAsk Ctxt m =>
  m { games :: Array Game }
list = do
  response <- request GET "/games" { body: Nothing :: Maybe {}, token: Nothing }
  decodeJson response.body # throwError <<< printJsonDecodeError \/ pure

submitTurn ::
  forall m.
  MonadAff m =>
  MonadThrow String m =>
  MonadAsk Ctxt m =>
  { token :: Token, positions :: Array Int, letters :: Array String, hand :: Array String } ->
  m { game :: Game }
submitTurn { token, positions, letters, hand } = do
  response <- request POST "/submit_turn" { body: Just { positions, letters, hand }, token: Just token }
  decodeJson response.body # throwError <<< printJsonDecodeError \/ pure

exchange ::
  forall m.
  MonadAff m =>
  MonadThrow String m =>
  MonadAsk Ctxt m =>
  { token :: Token, exchange_letters :: Array String, hand :: Array String } ->
  m { game :: Game }
exchange { token, exchange_letters, hand } = do
  response <- request POST "/exchange" { body: Just { exchange_letters, hand }, token: Just token }
  decodeJson response.body # throwError <<< printJsonDecodeError \/ pure
