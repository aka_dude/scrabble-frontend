module Main
  ( main
  ) where

import Prelude

import Api (Ctxt, joinGame, newGame, showGame, startGame) as Api
import Api (GameId(..), Token)
import Control.Monad.Except (ExceptT, runExceptT, throwError)
import Control.Monad.Reader (class MonadAsk, runReaderT)
import Control.Monad.Rec.Class (untilJust)
import Control.Monad.State (class MonadState, StateT, gets, modify_, runStateT)
import Control.Monad.Trans.Class (class MonadTrans, lift)
import Data.Argonaut as Agronaut
import Data.DList as DList
import Data.Either (Either(..))
import Data.Foldable (foldMap, for_)
import Data.Lens (_Just)
import Data.Map (Map)
import Data.Map as Map
import Data.Maybe (Maybe(..))
import Data.String (null) as String
import Data.Tuple.Nested ((/\))
import Data.Var (($=), (%))
import Data.Var (get) as Var
import Data.Var.Web (at, localStorage, location, origin, window)
import Data.Var.Web (pathname) as Var
import Effect (Effect)
import Effect.Aff.Class (class MonadAff)
import Effect.Class (class MonadEffect)
import Effect.Class.Console (error, logShow)
import Field (State(..), component, emptyFieldChanges, render) as Field
import Halogen as H
import Halogen.Aff as HA
import Halogen.Error (_error, _message)
import Halogen.Error (handleAction, render, wrapHalogen) as HE
import Halogen.HTML (a, b_, br_, button, div, div_, h1, input, slot) as HH
import Halogen.HTML.Core (renderWidget, text) as HH
import Halogen.HTML.Events (onClick, onValueChange) as HE
import Halogen.HTML.Properties as HP
import Halogen.VDom.Driver (runUI)
import Route (index, game, parse, render) as Route
import Type.Prelude (Proxy(..))
import Types (Action(..), Phase(..), Slots, State, phaseTokenAndOwner, setRoute)
import Utils (decodeAndParse, delayOnNothing, intercalateMap, matchF, nothing)

main :: Effect Unit
main = do
  pathname <- Var.get $ window % location % Var.pathname
  route <- case Route.parse pathname of
    Right route -> pure route
    Left errs -> do
      error $ "error(s) parsing route: " <> pathname <> foldMap ("\n  " <> _) errs
      pure Route.index
  origin <- Var.get $ window % location % origin
  let
    ctxt = { api: { root: origin <> "/api/v1" } }
  HA.runHalogenAff do
    phase <- matchF route
      { index: \{} -> pure $ CreatingGame { nickname: "" }
      , game: \gameId -> withStatePerGame do
          let defPhase = JoiningGame { gameId, nickname: "" }
          gets (Map.lookup gameId) >>= case _ of
            Nothing -> pure defPhase
            Just { token, nickname, owner } -> do
              flip runReaderT ctxt do
                runExceptT (Api.showGame { token }) >>= case _ of
                  Left err -> do
                    error $ "Error quering info about token " <> show token <> ": " <> err
                    pure defPhase
                  Right { game } -> case game.game_state of
                    "in_lobby" -> pure $ InLobby { token, nickname, gameId, owner, pollerId: Nothing }
                    "players_turn" -> pure $ Playing { token, nickname, gameId, owner }
                    "game_ended" -> pure $ GameEnded $ Field.State
                      { error: Nothing
                      , origin
                      , token
                      , nickname
                      , pollerId: Nothing
                      , game: Just game
                      , pickedLetter: nothing
                      , hand: DList.empty
                      , discardedLetters: DList.empty
                      , fieldChanges: Field.emptyFieldChanges
                      }
                    other -> do
                      error $ "Unknown game_state: " <> other
                      -- modify_ $ Map.delete gameId
                      pure defPhase
      }
    logShow phase
    body <- HA.awaitBody
    runUI (H.hoist (flip runReaderT ctxt) component)
      { error: Nothing, loading: 0, origin, game: Nothing, phase }
      body

withStatePerGame ::
  forall m a.
  MonadEffect m =>
  StateT (Map GameId { token :: Token, nickname :: String, owner :: Boolean }) m a ->
  m a
withStatePerGame m = do
  s <- Var.get (window % localStorage % at "state_per_game") >>= map decodeAndParse >>> case _ of
    Nothing -> pure Map.empty
    Just (Left err) -> do
      error $ "error parsing localStorage.state_per_game: " <> show err
      pure Map.empty
    Just (Right ok) -> pure ok
  result /\ s' <- runStateT m s
  window % localStorage % at "state_per_game" $= Just (Agronaut.stringify $ Agronaut.encodeJson s')
  pure result

component :: forall query m. MonadAff m => MonadAsk Api.Ctxt m => H.Component query State Void m
component =
  H.mkComponent
    { initialState: identity
    , render: HE.render (_error <<< _Just <<< _message) render
    , eval: H.mkEval H.defaultEval
        { handleAction = HE.handleAction handleAction, initialize = Just Empty }
    }

render ::
  forall m.
  MonadAff m =>
  MonadAsk Api.Ctxt m =>
  State ->
  H.ComponentHTML Action Slots m
render { origin, phase, game } = case phase of
  CreatingGame { nickname } -> HH.div [ HP.id "small-center-column" ]
    [ HH.h1 [ HP.style "text-align: center; margin-bottom: 10px;" ] [ HH.text "Scrabble" ]
    , HH.input
        [ HP.style "width: 300px;"
        , HP.placeholder "your nickname"
        , HP.value nickname
        , HE.onValueChange \s -> A do
            modify_ _ { phase = CreatingGame { nickname: s } }
        ]
    , HH.button
        [ HP.style "width: 300px;"
        , HE.onClick \_ -> CreateGame
        ]
        [ HH.text "Create game" ]
    , HH.div [ HP.style "text-align: center; margin-top: 10px;" ]
        [ HH.text "To join a game, follow a link created by game host"
        , HH.br_
        , HH.text "e.g. "
        , HH.b_ [ HH.text $ origin <> Route.render (Route.game (GameId 12345)) ]
        ]
    ]
  JoiningGame { nickname, gameId } -> HH.div [ HP.id "small-center-column" ]
    [ HH.h1 [ HP.style "text-align: center; margin-bottom: 10px;" ] [ HH.text "Scrabble" ]
    , HH.div_ [ HH.text $ "You are about to join game #" <> show gameId ]
    , HH.input
        [ HP.style "width: 300px; margin-top: 10px;"
        , HP.placeholder "your nickname"
        , HP.value nickname
        , HE.onValueChange \s -> A do
            modify_ _ { phase = JoiningGame { nickname: s, gameId } }
        ]
    , HH.div_
        [ HH.button [ HP.style "width: 150px;", HE.onClick \_ -> JoinGame ] [ HH.text "Join game" ]
        , HH.button [ HP.style "width: 150px;", HE.onClick \_ -> CreateGame ] [ HH.text "Create game" ]
        ]
    ]
  InLobby { gameId, owner } -> HH.div [ HP.id "small-center-column" ]
    [ HH.h1 [ HP.style "text-align: center; margin-bottom: 10px;" ] [ HH.text "Scrabble" ]
    , HH.div [ HP.style "margin-bottom: 10px;" ]
        [ HH.text "Other players may join via this link: "
        , let
            url = origin <> Route.render (Route.game gameId)
          in
            HH.a [ HP.href url ] [ HH.text url ]
        ]
    , HH.div [ HP.style "margin-bottom: 10px;" ] $ pure $ HH.text $ "Players in lobby: " <> case game of
        Nothing -> ""
        Just { players } -> intercalateMap ", " _.nickname players
    , if owner then HH.button [ HP.style "width: 300px;", HE.onClick \_ -> StartGame ]
        [ HH.text "Start game" ]
      else HH.text "Wait until game owner starts the game"
    ]
  Playing { token, nickname } -> HH.slot (Proxy :: Proxy "field") unit Field.component
    { token, nickname, origin: origin }
    EndGame
  GameEnded s -> Field.render s # HH.renderWidget (const Empty) (const $ HH.text "")

loading :: forall r m a. MonadState { loading :: Int | r } m => m a -> m a
loading m = do
  modify_ \s -> s { loading = s.loading + 1 }
  x <- m
  modify_ \s -> s { loading = s.loading - 1 }
  pure x

handleAction ::
  forall m.
  MonadAff m =>
  MonadAsk Api.Ctxt m =>
  Action ->
  ExceptT String (H.HalogenM State Action Slots Void m) Unit
handleAction action = loading case action of
  A a -> lift a
  Empty -> H.gets _.phase >>= case _ of
    InLobby phase@{ pollerId: Nothing } -> do
      pollerId <- spawnPoller
      modify_ _ { phase = InLobby phase { pollerId = Just pollerId } }
    _ -> pure unit
  CreateGame -> H.gets _.phase >>= case _ of
    CreatingGame { nickname } -> do
      when (String.null nickname) do
        throwError "nickname must not be an empty string"
      loading do
        { game, token } <- Api.newGame { nickname }
        logShow game
        withStatePerGame do
          modify_ $ Map.insert game.id { nickname, token, owner: true }
        pollerId <- spawnPoller
        modify_ _
          { phase = InLobby { gameId: game.id, token, nickname, owner: true, pollerId: Just pollerId } }
        setRoute $ Route.game game.id
    other -> throwError $ "App error: invalid phase/action: phase = " <> show (other)
      <> ", action = "
      <> show
        action
  JoinGame -> H.gets _.phase >>= case _ of
    JoiningGame { nickname, gameId } -> do
      when (String.null nickname) do
        throwError "nickname must not be an empty string"
      loading do
        { game, token } <- Api.joinGame { nickname, gameId }
        logShow game
        withStatePerGame do
          modify_ $ Map.insert game.id { nickname, token, owner: false }
        pollerId <- spawnPoller
        modify_ _
          { phase = InLobby { token, nickname, gameId: game.id, owner: false, pollerId: Just pollerId } }
        setRoute $ Route.game gameId
    other -> throwError $ "App error: invalid phase/action: phase = " <> show (other)
      <> ", action = "
      <> show
        action
  StartGame -> H.gets _.phase >>= case _ of
    InLobby { token, nickname, gameId, owner, pollerId } -> do
      loading do
        for_ pollerId $ lift <<< H.kill
        { game } <- Api.startGame { token, hand_size: Nothing }
        logShow game
        modify_ _ { phase = Playing { token, nickname, gameId, owner } }
    other -> throwError $ "App error: invalid phase/action: phase = " <> show (other)
      <> ", action = "
      <> show
        action
  EndGame _ -> pure unit

-- parse_istoredPhase ::
--   forall m.
--   MonadAff m =>
--   MonadAsk Api.Ctxt m =>
--   StoredPhase ->
--   ExceptT String (H.HalogenM State Action Slots Void m) Phase
-- parse_istoredPhase = case _ of
--   SCreatingGame inner -> pure $ CreatingGame inner
--   SJoiningGame inner -> pure $ JoiningGame inner
--   SInLobby inner -> ado
--     pollerId <- spawnPoller
--     in InLobby $ Record.insert (Proxy :: Proxy "pollerId") pollerId inner
--   SPlaying inner -> pure $ Playing inner

spawnPoller ::
  forall t m.
  MonadTrans t =>
  MonadAff m =>
  MonadAsk Api.Ctxt m =>
  t (H.HalogenM State Action Slots Void m) H.ForkId
spawnPoller = lift $ H.fork do
  HE.wrapHalogen (A (pure unit)) do
    pollForGameState

pollForGameState ::
  forall m.
  MonadAff m =>
  MonadAsk Api.Ctxt m =>
  ExceptT String (H.HalogenM State Action Slots Void m) Unit
pollForGameState = untilJust $ delayOnNothing do
  state <- H.get
  case phaseTokenAndOwner state.phase of
    Nothing -> pure Nothing
    Just { token } -> do
      { game } <- Api.showGame { token }
      modify_ _ { game = Just game }
      case state.phase of
        InLobby { gameId, nickname, owner } -> case game.game_state of
          "in_lobby" -> pure Nothing
          "players_turn" -> do
            modify_ _ { phase = Playing { gameId, token, nickname, owner } }
            pure $ Just unit
          _ -> throwError $ "pollForGameState: Unexpected or unknown game_state: " <> game.game_state
            <> " (phase = "
            <> show state.phase
        _ -> pure Nothing
