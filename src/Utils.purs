module Utils where

import Prelude

import Control.Alt (class Alt, (<|>))
import Control.Alternative (empty)
import Control.Monad.State (class MonadState, StateT(..), gets, put)
import Control.Plus (class Plus)
import Data.Argonaut (class DecodeJson, JsonDecodeError, decodeJson, parseJson)
import Data.Array (alterAt, unsafeIndex)
import Data.Array as Array
import Data.Array.Extra.Unsafe (unsafeUpdateAt)
import Data.Either (Either)
import Data.Foldable (class Foldable, foldl, intercalate)
import Data.FunctorWithIndex (class FunctorWithIndex, mapWithIndex)
import Data.Lens (Iso', Lens', lens, re, (.~), (^.))
import Data.Maybe (Maybe(..), fromJust)
import Data.String (CodePoint)
import Data.String as String
import Data.Tuple.Nested ((/\))
import Data.Unfoldable (class Unfoldable, none, singleton)
import Data.Variant (class VariantMatchCases, Variant)
import Data.Variant as Variant
import Debug (class DebugWarning, trace)
import Effect.Aff (Milliseconds(..))
import Effect.Aff as Aff
import Effect.Aff.Class (class MonadAff, liftAff)
import Partial.Unsafe (unsafeCrashWith, unsafePartial)
import Prim.Row (class Union)
import Prim.RowList (class RowToList, RowList)
import Type.Prelude (Proxy(..))

delayOnNothing :: forall a m. MonadAff m => m (Maybe a) -> m (Maybe a)
delayOnNothing m = m >>= case _ of
  Nothing -> liftAff $ Nothing <$ Aff.delay (Milliseconds 1000.0)
  Just ok -> pure $ Just ok

-- Debug utils

traceShowId :: forall a. DebugWarning => Show a => a -> a
traceShowId x = trace (show x) \_ -> x

emptyUnless :: forall f a. Applicative f => Plus f => Boolean -> a -> f a
emptyUnless true x = pure x
emptyUnless false _ = empty

-- Array utils

unsafeIx :: forall a. Int -> Lens' (Array a) a
unsafeIx i = lens getter setter
  where
  getter xs = unsafePartial $ xs ! i
  setter xs x = unsafePartial $ unsafeUpdateAt i x xs

unsafeAlterAt :: forall a. Partial => Int -> (a -> Maybe a) -> Array a -> Array a
unsafeAlterAt i f = fromJust <<< alterAt i f

infixl 8 unsafeIndex as !

mapWithIndexFlipped :: forall i f a b. FunctorWithIndex i f => f a -> (i -> a -> b) -> f b
mapWithIndexFlipped = flip mapWithIndex

infixr 1 mapWithIndexFlipped as <#!>

intercalateMap :: forall m f a. Monoid m => Functor f => Foldable f => m -> (a -> m) -> f a -> m
intercalateMap sep f = intercalate sep <<< map f

chunks :: forall a. Int -> Array a -> Array (Array a)
chunks 0 _ = unsafeCrashWith "Utils.chunks: expected non-zero chunk length"
chunks _ [] = []
chunks i xs = let { before, after } = Array.splitAt i xs in before `Array.cons` chunks i after

partition1Map ::
  forall f a b. Foldable f => Unfoldable f => Alt f => (a -> Maybe b) -> f a -> { yes :: Maybe b, no :: f a }
partition1Map f = foldl step { yes: Nothing, no: none }
  where
  step { yes: Nothing, no } x = case f x of
    Just y -> { yes: Just y, no }
    Nothing -> { yes: Nothing, no: no <|> singleton x }
  step { yes, no } x = { yes, no: no <|> singleton x }

partition1 ::
  forall f a. Foldable f => Unfoldable f => Alt f => (a -> Boolean) -> f a -> { yes :: Maybe a, no :: f a }
partition1 f = partition1Map \x -> if f x then Just x else Nothing

-- String utils

shead :: String → Maybe CodePoint
shead = map _.head <<< String.uncons

unsafeHead :: String → CodePoint
unsafeHead s = unsafePartial $ fromJust $ shead s

-- Variant utils

matchF ::
  forall (a :: Type) (rt :: RowList Type) (r :: Row Type) (v' :: Row Type) (v :: Row Type).
  RowToList r rt ⇒
  VariantMatchCases rt v' a ⇒
  Union v' () v ⇒
  Variant v →
  Record r →
  a
matchF = flip Variant.match

nothing :: forall r. Variant (nothing :: {} | r)
nothing = Variant.inj (Proxy :: Proxy "nothing") {}

just :: forall a r. a -> Variant (just :: a | r)
just = Variant.inj (Proxy :: Proxy "just")

isNothingV :: forall r a. Variant (nothing :: a | r) -> Boolean
isNothingV = Variant.on (Proxy :: Proxy "nothing") (const true) (const false)

-- State utils

alterStateT :: forall s s' a m. MonadState s' m => Iso' s s' -> StateT s m a -> m a
alterStateT i (StateT a) = do
  result /\ state <- gets (_ ^. re i) >>= a
  put $ state ^. i
  pure result

-- JSON

decodeAndParse :: forall a. DecodeJson a => String -> Either JsonDecodeError a
decodeAndParse = decodeJson <=< parseJson

-- Lens

swap :: forall s a. Lens' s a -> Lens' s a -> s -> s
swap x y s = s # x .~ (s ^. y) # y .~ (s ^. x)
