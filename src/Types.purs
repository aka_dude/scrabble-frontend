module Types where

import Prelude

import Api (Game) as Api
import Api (GameId, Token)
import Data.Argonaut (class DecodeJson, class EncodeJson, JsonDecodeError(..), caseJsonObject, caseJsonString, decodeJson, encodeJson, (.:))
import Data.Argonaut (fromObject, fromString) as Json
import Data.Either (Either(..))
import Data.Foldable (foldMap)
import Data.Generic.Rep (class Generic)
import Data.Maybe (Maybe(..))
import Data.Show.Generic (genericShow)
import Data.Var (($=), (%))
import Data.Var (get) as Var
import Data.Var.Web (location, window)
import Data.Var.Web (pathname) as Var
import Effect.Class (class MonadEffect, liftEffect)
import Effect.Class.Console (error)
import Effect.Exception as Effect
import Field (State) as Field
import Halogen as H
import Record (delete, merge) as Record
import Route (Route)
import Route (parse, render) as Route
import Type.Prelude (Proxy(..))

type Slots = (field :: forall query. H.Slot query Field.State Unit)

type State = Record StateR

type StateR =
  ( error :: Maybe { message :: String, action :: Action }
  , loading :: Int
  , origin :: String
  , game :: Maybe Api.Game
  , phase :: Phase
  )

-- data StatePhase
--   = Stored StoredPhase
--   | Active Phase

-- derive instance Generic StatePhase _
-- instance Show StatePhase where
--   show = genericShow

data Phase
  = CreatingGame { nickname :: String } -- /
  | JoiningGame { nickname :: String, gameId :: GameId } -- /join/:gameId
  | InLobby
      { token :: Token, nickname :: String, gameId :: GameId, owner :: Boolean, pollerId :: Maybe H.ForkId } -- /lobby/:gameId
  | Playing { token :: Token, nickname :: String, gameId :: GameId, owner :: Boolean }
  | GameEnded Field.State

instance Show Phase where
  show (CreatingGame _) = "CreatingGame"
  show (JoiningGame _) = "JoiningGame"
  show (InLobby _) = "InLobby"
  show (Playing _) = "Playing"
  show (GameEnded _) = "GameEnded"

-- phaseNickname :: StoredPhase -> String
-- phaseNickname (SCreatingGame { nickname }) = nickname
-- phaseNickname (SJoiningGame { nickname }) = nickname
-- phaseNickname (SInLobby { nickname }) = nickname
-- phaseNickname (SPlaying { nickname }) = nickname

phaseTokenAndOwner :: Phase -> Maybe { token :: Token, owner :: Boolean }
phaseTokenAndOwner (CreatingGame {}) = Nothing
phaseTokenAndOwner (JoiningGame {}) = Nothing
phaseTokenAndOwner (InLobby { token, owner }) = Just { token, owner }
phaseTokenAndOwner (Playing { token, owner }) = Just { token, owner }
phaseTokenAndOwner (GameEnded _) = Nothing

-- phaseTokenAndOwner :: StoredPhase -> Maybe { token :: Token, owner :: Boolean }
-- phaseTokenAndOwner (SCreatingGame {}) = Nothing
-- phaseTokenAndOwner (SJoiningGame {}) = Nothing
-- phaseTokenAndOwner (SInLobby { token, owner }) = Just { token, owner }
-- phaseTokenAndOwner (SPlaying { token, owner }) = Just { token, owner }

-- data StoredPhase
--   = SCreatingGame { nickname :: String }
--   | SJoiningGame { nickname :: String, gameId :: GameId }
--   | SInLobby { token :: Token, nickname :: String, gameId :: GameId, owner :: Boolean }
--   | SPlaying { token :: Token, nickname :: String, gameId :: GameId, owner :: Boolean }

-- derive instance Generic StoredPhase _

-- instance Show StoredPhase where
--   show = genericShow

-- instance EncodeJson StoredPhase where
--   encodeJson (SCreatingGame inner) = encodeJson $ Record.merge { phase: "CreatingGame" } inner
--   encodeJson (SJoiningGame inner) = encodeJson $ Record.merge { phase: "JoiningGame" } inner
--   encodeJson (SInLobby inner) = encodeJson $ Record.merge { phase: "InLobby" } inner
--   encodeJson (SPlaying inner) = encodeJson $ Record.merge { phase: "Playing" } inner

-- instance DecodeJson StoredPhase where
--   decodeJson = caseJsonObject (Left (TypeMismatch "Phase (object)")) \o -> do
--     o .: "phase" >>= caseJsonString (Left (AtKey "phase" (TypeMismatch "string"))) case _ of
--       "CreatingGame" -> SCreatingGame <$> decodeJson (Json.fromObject o)
--       "JoiningGame" -> SJoiningGame <$> decodeJson (Json.fromObject o)
--       "InLobby" -> SInLobby <$> decodeJson (Json.fromObject o)
--       "Playing" -> SPlaying <$> decodeJson (Json.fromObject o)
--       other -> Left (UnexpectedValue (Json.fromString other))

-- render_istoredPhase :: Phase -> StoredPhase
-- render_istoredPhase = case _ of
--   CreatingGame inner -> SCreatingGame inner
--   JoiningGame inner -> SJoiningGame inner
--   InLobby inner -> SInLobby $ Record.delete (Proxy :: Proxy "pollerId") inner
--   Playing inner -> SPlaying inner

data Action
  = A (forall m. H.HalogenM State Action Slots Void m Unit)
  | Empty
  | CreateGame
  | JoinGame
  | StartGame
  | EndGame Field.State

instance Show Action where
  show (A _) = "A _"
  show Empty = "Empty"
  show CreateGame = "CreateGame"
  show JoinGame = "JoinGame"
  show StartGame = "StartGame"
  show (EndGame _) = "EndGame"

setRoute :: forall m. MonadEffect m => Route -> m Unit
setRoute route = do
  pathname <- Var.get (window % location % Var.pathname)
  prevRoute <- case Route.parse pathname of
    Left errs -> do
      error $ "error(s) parsing route: " <> pathname <> foldMap ("\n  " <> _) errs
      pure route
    Right ok -> pure ok
  when (route /= prevRoute) do
    window % location % Var.pathname $= Route.render route
    liftEffect do
      Effect.throw "Changing pathname... (this exception is intentional)"

-- setPhase :: forall m r. MonadEffect m => MonadState { phase :: StatePhase | r } m => Phase -> m Unit
-- setPhase phase = do
--   H.modify_ $ _ { phase = Active phase }
--   window % localStorage % at "phase" $= Just (Json.stringify (encodeJson (render_istoredPhase phase)))
