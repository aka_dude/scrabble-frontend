module Field where

import Prelude

import Api (PlayerId(..), Token)
import Api as Api
import Control.Monad.Except (ExceptT, throwError)
import Control.Monad.Reader (class MonadAsk)
import Control.Monad.Rec.Class (untilJust)
import Data.Array (catMaybes, concat, cons, replicate, (..))
import Data.Array as Array
import Data.Array.Extra.Unsafe (unsafeModifyAt, unsafeUpdateAt)
import Data.DList (DList)
import Data.DList (after, elems, empty, front, mut, pushBack) as DList
import Data.Foldable (any, elem, foldMap, foldl, intercalate, null)
import Data.Functor (voidRight)
import Data.FunctorWithIndex (mapWithIndex)
import Data.Lens (ALens', Lens', _1, _2, _Just, cloneLens, traversed, (%~), (^.))
import Data.Lens.Iso.Newtype (_Newtype)
import Data.Lens.Record as Lens
import Data.List.Lazy (List)
import Data.Maybe (Maybe(..), fromJust, isJust)
import Data.MultiSet (difference, fromFoldable, toUnfoldable) as MultiSet
import Data.Newtype (class Newtype, unwrap)
import Data.Newtype (modify) as Newtype
import Data.String (CodePoint)
import Data.String (singleton) as String
import Data.Tuple as Tuple
import Data.Tuple.Nested (type (/\), (/\))
import Data.Variant (Variant, match)
import Data.Variant (inj) as Variant
import Effect.Aff.Class (class MonadAff)
import Effect.Class (liftEffect)
import Effect.Class.Console (warn)
import Effect.Unsafe (unsafePerformEffect)
import Halogen as H
import Halogen.Error (_error, _message)
import Halogen.Error (handleActionNoAction, render, wrapHalogenNoAction) as HE
import Halogen.HTML as HH
import Halogen.HTML.Events (onClick, onDragEnd, onDragOver, onDragStart, onDrop) as HE
import Halogen.HTML.Properties as HP
import Partial.Unsafe (unsafePartial)
import Type.Prelude (Proxy(..))
import Utils (delayOnNothing, isNothingV, just, matchF, nothing, swap, unsafeHead, unsafeIx, (!), (<#!>))
import Web.Event.Event (preventDefault) as Web
import Web.HTML.Event.DragEvent (DragEvent)
import Web.HTML.Event.DragEvent as Drag
import Web.HTML.HTMLAudioElement as Audio
import Web.HTML.HTMLMediaElement as Media

component :: forall query m. MonadAff m => MonadAsk Api.Ctxt m => H.Component query Input Output m
component =
  H.mkComponent
    { initialState
    , render: HE.render (_Newtype <<< _error <<< _Just <<< _message) render
    , eval: H.mkEval H.defaultEval
        { handleAction = HE.handleActionNoAction (_Newtype <<< _error) handleAction
        , initialize = Just aEmpty
        }
    }

type Letter = CodePoint

type Coord = Int /\ Int

type Input = { token :: Token, nickname :: String, origin :: String }

type Output = State

newtype State = State StateI

type StateI =
  { error :: Maybe { message :: String }
  , origin :: String
  , token :: Token
  , nickname :: String
  , pollerId :: Maybe H.ForkId
  , game :: Maybe Api.Game
  , pickedLetter ::
      Variant
        ( nothing :: {}
        , just :: { source :: ALens' State (Maybe Letter) }
        )
  , hand :: DList Letter
  , discardedLetters :: DList Letter
  , fieldChanges :: Array (Array (Maybe Letter))
  }

derive instance Newtype State _

instance Show State where
  show (State { error }) = show { error }

initialState :: Input -> State
initialState { token, nickname, origin } = State
  { error: Nothing
  , origin
  , pollerId: Nothing
  , token
  , nickname
  , game: Nothing
  , pickedLetter: nothing
  , hand: DList.empty
  , discardedLetters: DList.empty
  , fieldChanges: emptyFieldChanges
  }

emptyFieldChanges :: forall a. Array (Array (Maybe a))
emptyFieldChanges = replicate Api.fieldSide $ replicate Api.fieldSide Nothing

type Action = Variant
  ( aOnDragStart :: { source :: ALens' State (Maybe Letter) }
  , aOnDrop :: { target :: ALens' State (Maybe Letter) }
  , aFieldOnClick :: { coord :: Coord }
  , aOnDragEnd :: {}
  , aEndTurn :: {}
  , aEmpty :: {}
  )

aOnDragStart :: forall a r. a -> Variant (aOnDragStart :: a | r)
aOnDragStart = Variant.inj (Proxy :: Proxy "aOnDragStart")

aOnDrop :: forall a r. a -> Variant (aOnDrop :: a | r)
aOnDrop = Variant.inj (Proxy :: Proxy "aOnDrop")

aFieldOnClick :: forall a r. a -> Variant (aFieldOnClick :: a | r)
aFieldOnClick = Variant.inj (Proxy :: Proxy "aFieldOnClick")

aOnDragEnd :: forall r. Variant (aOnDragEnd :: {} | r)
aOnDragEnd = Variant.inj (Proxy :: Proxy "aOnDragEnd") {}

aEndTurn :: forall r. Variant (aEndTurn :: {} | r)
aEndTurn = Variant.inj (Proxy :: Proxy "aEndTurn") {}

aEmpty :: forall r. Variant (aEmpty :: {} | r)
aEmpty = Variant.inj (Proxy :: Proxy "aEmpty") {}

handleAction ::
  forall slots m.
  MonadAff m =>
  MonadAsk Api.Ctxt m =>
  Action ->
  ExceptT String (H.HalogenM State Action slots Output m) Unit
handleAction = match
  { aEmpty: \{} -> H.gets (unwrap >>> _.pollerId) >>= case _ of
      Just _ -> pure unit
      Nothing -> do
        pollerId <- spawnPoller
        H.modify_ $ Newtype.modify _ { pollerId = Just pollerId }
  , aOnDragStart: \{ source } -> H.modify_ $ _Newtype _ { pickedLetter = just { source } }
  , aOnDrop: \{ target } -> H.modify_ \s -> matchF (unwrap s).pickedLetter
      { nothing: \{} -> s
      , just: \{ source } ->
          let
            source' = cloneLens source
            target' = cloneLens target
          in
            s # swap source' target'
      }
  , aFieldOnClick: \{ coord: i /\ j } -> H.modify_ $ Newtype.modify \s ->
      case unsafePartial $ s.fieldChanges ! i ! j of
        Just letter -> unsafePartial s
          { hand = DList.pushBack letter s.hand
          , fieldChanges = s.fieldChanges # unsafeModifyAt i (unsafeUpdateAt j Nothing)
          }
        _ -> s
  , aEndTurn: \{} -> do
      State { token, fieldChanges, hand, discardedLetters } <- H.get
      H.modify_ $ _Newtype _ { error = Nothing }
      let
        hasPutLettersOnField = fieldChanges # any (any isJust)
        hasDiscardedLetters = not $ null discardedLetters
      when (hasPutLettersOnField && hasDiscardedLetters) do
        throwError "You cannot both discard letters and put some on the board"
      when (not hasPutLettersOnField && not hasDiscardedLetters) do
        throwError "You must either discard letters or put some on the board"
      if hasPutLettersOnField then do
        let
          positions = catMaybes $ concat $ fieldChanges <#!> \i -> mapWithIndex \j -> voidRight $
            i * Api.fieldSide + j
          letters = catMaybes $ concat $ fieldChanges <#> map (map String.singleton)
        void $ Api.submitTurn
          { token, positions, letters, hand: Array.fromFoldable hand <#> String.singleton }
      else do
        void $ Api.exchange
          { token
          , hand: Array.fromFoldable hand <#> String.singleton
          , exchange_letters: Array.fromFoldable discardedLetters <#> String.singleton
          }
      H.modify_ $ _Newtype _
        { fieldChanges = emptyFieldChanges
        , discardedLetters = DList.empty
        }
  , aOnDragEnd: \{} -> H.modify_ $ Newtype.modify \s -> s { pickedLetter = nothing }
  }

spawnPoller ::
  forall slots m.
  MonadAff m =>
  MonadAsk Api.Ctxt m =>
  ExceptT String (H.HalogenM State Action slots Output m) H.ForkId
spawnPoller = H.lift $ H.fork $ HE.wrapHalogenNoAction do
  poller

poller ::
  forall action slots m.
  MonadAff m =>
  MonadAsk Api.Ctxt m =>
  ExceptT String (H.HalogenM State action slots Output m) Unit
poller = do
  do
    initGame <- fetchGameAndUpdate
    State state <- H.get
    let
      me = unsafePartial $ fromJust $ initGame.players # Array.find (eq state.nickname <<< _.nickname)
    H.modify_ $ Newtype.modify _ { hand = Array.toUnfoldable $ unsafeHead <$> me.hand }

  untilJust $ delayOnNothing do
    State statePrev <- H.get
    newGame <- fetchGameAndUpdate
    case newGame.game_state of
      "game_ended" -> do
        H.lift <<< H.raise =<< H.get
        pure $ Just unit
      _ -> do
        State state <- H.get
        when (not (isMyTurn statePrev) && isMyTurn state) do
          liftEffect do
            Media.play <<< Audio.toHTMLMediaElement =<< Audio.create' "/cuckoo.mp3"
        unless (isMyTurn state) do
          let
            me = unsafePartial $ fromJust $ newGame.players # Array.find (eq state.nickname <<< _.nickname)
            newLetters = MultiSet.fromFoldable (me.hand <#> unsafeHead) `MultiSet.difference`
              MultiSet.fromFoldable state.hand
            lostLetters = MultiSet.fromFoldable state.hand `MultiSet.difference` MultiSet.fromFoldable
              (me.hand <#> unsafeHead)
          if not (null lostLetters) then do
            warn $ "Lost letters! " <> intercalate ", "
              (String.singleton <$> MultiSet.toUnfoldable lostLetters :: List _)
            H.modify_ $ Newtype.modify _ { hand = Array.toUnfoldable $ unsafeHead <$> me.hand }
          else do
            H.modify_ $ Newtype.modify _ { hand = foldl (flip DList.pushBack) state.hand newLetters }
        pure Nothing

fetchGameAndUpdate ::
  forall action slots output m.
  MonadAff m =>
  MonadAsk Api.Ctxt m =>
  ExceptT String (H.HalogenM State action slots output m) Api.Game
fetchGameAndUpdate = do
  State state <- H.get
  { game } <- Api.showGame { token: state.token }
  H.modify_ $ Newtype.modify _ { game = Just game }
  pure game

render :: forall slots m. State -> H.ComponentHTML Action slots m
render (State state) = HH.div [ HP.id "columns" ]
  [ HH.div_
      [ HH.a [ HP.href $ state.origin ] [ HH.h2_ [ HH.text "Scrabble" ] ]
      ]
  , HH.div [ HP.id "main-column" ]
      [ rfield state
      , rhand state
      , HH.div [ HP.style "flex-grow: 1;" ] []
      , rdiscardSpace state
      ]
  , HH.div_ $
      [ case state.game of
          Nothing -> HH.text "loading game..."
          Just game ->
            HH.table [ HP.id "scoreboard" ] $ pure $ HH.tbody_ $
              [ HH.tr_ [ HH.th_ [ HH.text "score" ], HH.th_ [ HH.text "nickname" ] ] ] <>
                ( game.players <#!>
                    \i { nickname, score } -> HH.tr_
                      [ HH.td_ [ HH.text $ show score ]
                      , HH.td_
                          [ case game.game_state of
                              "players_turn" | i == game.players_turn -> HH.u_ [ HH.text nickname ]
                              "game_ended" | elem (PlayerId i) game.winners -> HH.text $ "🏆 " <> nickname
                              _ -> HH.text nickname
                          ]
                      ]
                )
      ]
        <>
          ( if isMyTurn state then [ HH.button [ HE.onClick \_ -> aEndTurn ] [ HH.text "End turn" ] ]
            else []
          )
        <>
          flip foldMap state.game \game ->
            if game.game_state == "game_ended" then
              [ HH.h3 [ HP.style "margin-top: 20px;" ] [ HH.text "Game has ended" ] ]
            else []
  ]

isMyTurn :: StateI -> Boolean
isMyTurn { game, nickname } = case game of
  Just g -> unsafePartial $ (g.players ! g.players_turn).nickname == nickname
  Nothing -> false

gameFieldOrEmpty :: StateI -> Array (Array (Maybe Letter))
gameFieldOrEmpty { game: Just game } = Api.gameField game.field
gameFieldOrEmpty { game: Nothing } = emptyFieldChanges

rfield :: forall slots m. StateI -> H.ComponentHTML Action slots m
rfield state = HH.div [ HP.id "field-wrapper" ] [ HH.table [ HP.id "field" ] [ HH.tbody_ rrows ] ]
  where
  rrows = gameFieldOrEmpty state <#!> rcols
  rcols i row = HH.tr_ $ row <#!> rcell i
  -- | Cell either empty or occupied by player's new letter
  rcell i j Nothing =
    HH.td
      ( [ HE.onDragOver onDragOverDefault
        , HE.onDrop $ fieldOnDrop i j
        ] <>
          ( modifier i j # foldMap \{ title, color } ->
              [ HP.title title, HP.style $ "background-color: " <> color ]
          )
      )
      case unsafePartial $ state.fieldChanges ! i ! j of
        -- | Cell is empty
        Nothing -> []
        -- | Cell is occupied by player's new letter
        Just letter ->
          [ HH.span
              [ HP.class_ cletter
              , HP.draggable true
              , HE.onDragStart $ fieldOnOnDragStart i j
              , HE.onDragEnd $ const aOnDragEnd
              , HE.onClick $ fieldOnClick i j
              ]
              [ HH.text $ String.singleton letter ]
          ]
  -- | Cell is occupied by an old letter
  rcell _ _ (Just letter) = HH.td [ HP.style "background-color: lightgrey;" ]
    [ HH.text $ String.singleton letter ]

  _fieldChanges :: Lens' State (Array (Array (Maybe Letter)))
  _fieldChanges = _Newtype <<< Lens.prop (Proxy :: Proxy "fieldChanges")

  _fieldChangesAt :: Int -> Int -> ALens' State (Maybe Letter)
  _fieldChangesAt i j = _fieldChanges <<< unsafeIx i <<< unsafeIx j

  fieldOnOnDragStart :: Int -> Int -> DragEvent -> Action
  fieldOnOnDragStart i j _e = aOnDragStart { source: _fieldChangesAt i j }

  fieldOnDrop i j _e = aOnDrop { target: _fieldChangesAt i j }

  fieldOnClick i j _e = aFieldOnClick { coord: i /\ j }

  modifier i j
    | i == Api.fieldSide `div` 2 && j == Api.fieldSide `div` 2 = Just center
    | elem i edgesAndHalf && elem j edgesAndHalf = Just word_x3
    | elem (i /\ j) wx2s = Just word_x2
    | elem (i /\ j) lx3s = Just letter_x3
    | elem (i /\ j) lx2s = Just letter_x2
    | otherwise = Nothing

  edgesAndHalf = [ 0, Api.fieldSide `div` 2, Api.fieldSide - 1 ]
  mirror f = traversed %~ mirrorOne f
  mirrorDiagonal = Tuple.swap
  mirrorOne f = f %~ ((Api.fieldSide - 1) - _)

  firstWX2Strip = 1 .. 4 <#> \i -> i /\ i
  wx2s =
    firstWX2Strip
      <> (firstWX2Strip # mirror _1)
      <> (firstWX2Strip # mirror _2)
      <> (firstWX2Strip # mirror _1 # mirror _2)

  firstLX3Cell = 1 /\ 5
  halfOfLX3s =
    [ firstLX3Cell
    , firstLX3Cell # mirrorOne _1
    , firstLX3Cell # mirrorOne _2
    , firstLX3Cell # mirrorOne _1 # mirrorOne _2
    ]
  lx3s = halfOfLX3s <> map mirrorDiagonal halfOfLX3s

  firstLX2Eigth = [ 0 /\ 3, 2 /\ 6, 3 /\ 7, 6 /\ 6 ]
  firstLX2Quadrant = firstLX2Eigth <> map mirrorDiagonal firstLX2Eigth
  lx2s = firstLX2Quadrant <> (firstLX2Quadrant # mirror _1) <> (firstLX2Quadrant # mirror _2) <>
    (firstLX2Quadrant # mirror _1 # mirror _2)

  center = { title: "center", color: "cyan" }
  word_x3 = { title: "word x3", color: "lightcoral" }
  word_x2 = { title: "word x2", color: "lightblue" }
  letter_x3 = { title: "letter x3", color: "palegoldenrod" }
  letter_x2 = { title: "letter x2", color: "palegreen" }

rhand :: forall slots m. StateI -> H.ComponentHTML Action slots m
rhand state = rdeck
  { id: "hand"
  , placeholder: "your hand is empty"
  , locked: not (isMyTurn state)
  , markDroppable: not $ isNothingV state.pickedLetter
  , deck: _Newtype <<< Lens.prop (Proxy :: Proxy "hand")
  , deckValue: state.hand
  }

rdiscardSpace :: forall slots m. StateI -> H.ComponentHTML Action slots m
rdiscardSpace state = rdeck
  { id: "discard-space"
  , placeholder: "to discard, drop letters here"
  , locked: not (isMyTurn state)
  , markDroppable: not $ isNothingV state.pickedLetter
  , deck: _Newtype <<< Lens.prop (Proxy :: Proxy "discardedLetters")
  , deckValue: state.discardedLetters
  }

rdeck ::
  forall slots m.
  { id :: String
  , placeholder :: String
  , locked :: Boolean
  , markDroppable :: Boolean
  , deck :: ALens' State (DList Letter)
  , deckValue :: DList Letter
  } ->
  H.ComponentHTML Action slots m
rdeck { id, placeholder, locked, markDroppable, deck, deckValue } =
  HH.div [ HP.id id, HP.class_ (HH.ClassName "deck") ] handItems
  where
  handItems = cons (rdropPlace DList.front) $ concat $ DList.elems deckValue <#> \x ->
    [ rletter (DList.mut x), rdropPlace (DList.after x) ]

  rdropPlace x = HH.span
    ( [ HP.classes $ map HH.ClassName $ [ "letter-drop-place" ] <>
          if markDroppable then [ "mark-droppable" ] else []
      ] <>
        if locked then []
        else [ HE.onDragOver onDragOverDefault, HE.onDrop \_ -> aOnDrop { target: cloneLens deck <<< x } ]
    )
    if null deckValue then [ HH.text placeholder ] else []

  rletter x =
    HH.span
      ( [ HP.classes $ [ cletter ] <> if locked then [ HH.ClassName "inactive" ] else [] ] <>
          if locked then
            []
          else
            [ HP.draggable true
            , HE.onDragStart \_ -> aOnDragStart { source: cloneLens deck <<< x }
            , HE.onDrop \_ -> aOnDrop { target: cloneLens deck <<< x }
            , HE.onDragEnd \_ -> aOnDragEnd
            , HE.onDragOver onDragOverDefault
            ]
      )
      [ HH.text $ String.singleton $ unsafePartial $ fromJust $ deckValue ^. cloneLens x ]

cletter :: HH.ClassName
cletter = HH.ClassName "letter"

onDragOverDefault :: forall r. DragEvent -> Variant (aEmpty :: {} | r)
onDragOverDefault e =
  let
    _ = unsafePerformEffect $ Web.preventDefault $ Drag.toEvent e
  in
    aEmpty
