## To serve frontend for development or build it, you need to install Nix first:

```bash
curl --proto '=https' --tlsv1.2 -sSf -L https://install.determinate.systems/nix | sh -s -- install
```

## Develop backend with running frontend

1. ```bash
   nix run git+https://codeberg.org/aka_dude/scrabble-frontend#backend-dev
   ```

2. Run backend at http://127.0.0.1:8081

## Build frontend for production

1. ```bash
   nix build git+https://codeberg.org/aka_dude/scrabble-frontend
   ```

2. ./result is a symbolic link pointing at build artefacts

# Backend

Backend sources can be found here: https://github.com/PlaksinAnton/Scrabbles-backend-api.

# Assets

Cuckoo clock sound was recorded by Herbert Boland and published at [freesound.org](https://freesound.org/people/HerbertBoland/sounds/28113/) under [CC BY 4.0 DEED](https://creativecommons.org/licenses/by/4.0/).
